const express=require("express");
const bodyParser=require("body-parser");
var fs=require("fs");
const app=express();
app.use(bodyParser.urlencoded({
    extended:true}
    ));

app.get('/',function(req,res){
    res.sendFile(__dirname + "/crud.html");

});
app.post("/adduser",function(req,res){
    var username=req.body.username;
    var dob=req.body.userdate;
    var place=req.body.userplace;
    var obj={};
    var key=req.body.uid;
    var newuser={
        "Name": username,
        "Dob": dob,
        "Place": place
    }
    obj[key]=newuser;

    fs.readFile("details.json",function(err,data){
        var data=JSON.parse(data);
        data[key]=obj[key];
        console.log(data);
        var updateuser=JSON.stringify(data);
        fs.writeFile("details.json",updateuser,function(err){
            res.end( JSON.stringify(data))
        });

    });

});
app.post("/delete",function(req,res){
    fs.readFile("details.json",function(err,data){
        var data=JSON.parse(data);
        delete data[req.body.uid];
        var tran=JSON.stringify(data);
        fs.writeFile("details.json",tran,function(err){
            res.end(JSON.stringify(data));
        
    });

});
});
app.post("/user",function(req,res){
    fs.readFile("details.json",function(err,data){
        var data=JSON.parse(data);
        var user=data[req.body.uid];
        res.end(JSON.stringify(user));
    });
});
app.post("/all",function(req,res){
fs.readFile("details.json",function(err,data){
    res.end(data);
});

});
app.listen(8081);

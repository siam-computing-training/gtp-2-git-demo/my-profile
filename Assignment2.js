
const express=require("express");
//import express
const app=express();
var bodyparser=require("body-parser");
//import body parser module
var multer=require("multer");
//import multer module for handling multipart form data
const Nodemailer=require("nodemailer");
//import nodemailer module

var to;
var subject;
var body;
var path;
//declare global variables

var Storage=multer.diskStorage({
    destination:function(req,file,callback){
        callback(null,"./public")
        //define storage for upload file in local file system
        // and store that in storage variable

    },
    filename:function(req,file,callback){
        callback(null,file.fieldname + "" + Date.now() + "" +file.originalname)
   //define name for uploading file in server(system)
    }

})
var upload=multer({

        storage:Storage
        //assign storage variable value that we already created

      }).single("image")//define single file attachment

app.use(bodyparser.urlencoded({extended:true}));
//using this middleware function acces data inside body

app.get("/",function(req,res){

    res.sendFile(__dirname + "/userinfo.html");

    //send html file that we already created
})

app.post("/sendmail",function(req,res){

    upload(req,res,function(err){

        if(err)
        {
            return res.send("OOPS Something Went Wrong");
            //if get err send this message 
        }
        else

        {
    
            to=req.body.to;
            subject=req.body.subject;
            body=req.body.Body;
            path=req.file.path;
            
            //console.log(to);
            //console.log(subject);
            //console.log(path);

            //


           var transporter= Nodemailer.createTransport({
                service:"gmail",
                auth:{
                    user:"veeramaheshwaransiam@gmail.com",
                    pass:"password"                    
                }
                //specify gmail id and password
                // first we have to change settings in google account then only we can send mail
                //google account settings->security->turn on the less secure app access
            });
        
             var compose={

                 from:"veeramaheshwaransiam@gmail.com",//sender email
                 to:to,//receiver email
                 subject:subject,//subject of the mail
                 text:body,//body of the mail
                 
                 attachments:
                 [

                     {
                     path:path
                     }
                ]
             }

             transporter.sendMail(compose,function(err,info){

                if(err)
                {
                    console.log(err);
                    //show error in console
                }

                else{
                    console.log(info.response);//display information about sent mail in console
                    res.send("Mail Sent Successfully");

                    //send this message if mail sent successfully
                }



             })
            

            }
        })

    })


app.listen(8081);
//server running on port 8081

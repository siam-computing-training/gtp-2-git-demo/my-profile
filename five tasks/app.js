const express=require("express");
const app=express()
var router=express.Router();
var controller=require("./controller");
//employee details url
router.get("/getemployeedetails",controller.getemployee);
//employee status url
router.get("/employee_status",controller.getstatus);
//getemployee array url
router.get("/getemployee_array",controller.merge);
//highranked department url
router.get("/highranked",controller.highranked);
//gender total salary url
router.get("/gender_totalsalary",controller.totalsalary);
//popular email domain url
router.get("/popular_email_domain",controller.popularemail)
app.use("/employee",router);
app.listen(8089);

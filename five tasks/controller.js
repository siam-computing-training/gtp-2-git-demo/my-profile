const app=require("express")();
var connection=require("/Users/Admin/restapi/Mysql/connection");
var mydata=[];
//controller for fetching employee details from different tables
const getemployee=(req,res)=>{
var task1=new Promise((resolve,reject)=>{
var query="select employee.firstname,employee.lastname,employee.email,employee.gender,employee.mobile,employee.address,employee.city,employee.designation,department.name,department.status,salary.salary from employee inner join department on employee.department=department.id inner join salary on salary.employee_id=employee.id";
 connection.query(query,function(err,data){

            if(err) throw err;
            resolve(data)
        })
    })
    task1.then(output=>
        {
        res.json(output)
    })
    .catch(err=>{
        res.json({"Status":"false","Message":"OOPS Something Went Wrog"});
    })
    
}
//controller for employee status
var getstatus=(req,res)=>{
var employeestatus=new Promise((resolve,reject)=>{
var query='select employee.firstname,employee.lastname,employee.department,employee.designation,department.name,department.status as "department status",employee.status as "employess status" from employee inner join department on employee.status="active" and department.status="active" group by (employee.id) '
connection.query(query,function(err,data){

            if(err) throw err;
            resolve(data)
        })
    })
    employeestatus.then(data=>{
        res.json(data)
    })
    .catch(err=>{
        res.json({"Status":"false","Message":"OOPS Something Went Wrog"});
    })
}
//controller for converting array format 
var merge=(req,res)=>{
var array=new Promise((resolve,reject)=>{
var objdata={};
var query="select employee.firstname,employee.lastname,employee.email,employee.gender,employee.mobile,employee.address,employee.city,employee.designation,department.name,department.status,salary.salary from employee inner join department on employee.department=department.id inner join salary on salary.employee_id=employee.id";

connection.query(query,function(err,data){

        data.forEach(res=> {
            var data=JSON.parse(JSON.stringify(res));
            objdata.Name=data.firstname+" "+data.lastname;
            objdata.Designation=data.name+" "+data.designation;
            objdata.Salary=data.salary;
             objdata.Communication=data.address+" "+data.city+" "+data.email+" "+data.mobile;
             mydata.push(objdata);
            if(err) throw err;
            resolve(mydata)
            
        });
        
    })
    })
    array.then(data=>{
        res.json(data)
    })
    .catch(err=>{
        res.json({"Status":"false","Message":"OOPS Something Went Wrog"});
    })
}
//controller for high ranked salary based on department
var highranked=(req,res)=>{
var salary=new Promise((resolve,reject)=>{
var query='select employee.firstname,employee.lastname,employee.department,department.name,max(salary.salary) as maximum from employee inner join department on employee.department=department.id inner join salary on salary.employee_id=employee.id group by employee.department order by (max(salary.salary)) desc limit 1 '
connection.query(query,function(err,data){
    if(err) throw err;
    resolve(data)
})
    })
    salary.then(data=>{
        res.json(data);
    })
    .catch(err=>{
        res.json({"Status":"false","Message":"OOPS Something Went Wrog"});
    })
    
}
//controller total salary based on gender
var totalsalary=(req,res)=>{
var salary=new Promise((resolve,reject)=>{
var query='select employee.gender,sum(salary.salary) as total_salary from employee inner join salary on employee.id=salary.employee_id group by gender';
connection.query(query,function(err,data){
            if(err) throw err;
            resolve(data)

        })
    })
    
      salary.then(data=>{
          res.json(data)
      })
      .catch(err=>{
          res.json({"Status":"false","Message":"OOPS Something Went Wrog"});
      })
}
//controller for popular email domain
var popularemail=(req,res)=>{
    var Mail=new Promise((resolve,reject)=>{
        var query="SELECT substring_index(email, '@', -1) domain, COUNT(*) email_count from employee GROUP BY substring_index(email, '@', -1) ORDER BY email_count DESC LIMIT 1;"
        connection.query(query,function(err,data){
            if(err) throw err;
            resolve(data)
        })
    })
    Mail.then(data=>{res.json(data)})
    .catch(err=>{
        res.json({"Status":"false","Message":"Something Went Wrong"})
    })

    
}

module.exports={getemployee,getstatus,merge,highranked,totalsalary,popularemail};
//exporting modules for router
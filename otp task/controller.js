const model=require("./model.js").connection;
const bodyparser=require("body-parser")
const joi=require("joi");

// controller for Admin Login
const adminLogin=(req,res)=>
{
var user={"username":req.body.username,"password":req.body.password}
//request validation
var validate=(user)=>
{
  const joischema=joi.object(
  {
  username:joi.string().required(),
  password:joi.string().required()
  }).options({abortEarly:false});
  return joischema.validate(user);
}
response=validate(user);
if(response.error)
{
res.status(400).json({"Error":response.error.details[0].message})
}
else
{
// check user name and password
var promise1=()=>new Promise((resolve,reject)=>
{
model.query("select username,password from admin where username='"+req.body.username+"' and password='"+req.body.password+"'",(err,data)=>{
if(err) throw res.status(400).json({"Error":err});
if(data=="")
{
res.status(404).json({status:"false",Message:"Invalid Username or Passwword"})
}
else
{


resolve(data)
}
});
});
//user list
var promise2=()=>new Promise((resolve,reject)=>
{
model.query("select user_id,mobileno,status from user",(err,data)=>{
if(err) throw res.status(400).json({status:"false",Error:err})
resolve(data);
//if get success
});
});

promise1().then(promise2).then((output)=>
{
res.status(200).json({UserList:output})


}).catch(error=>res.status(400).json({Status:"false",Message:error}))
}
}

//controller for forgotpassword

const changePassword=(req,res)=>
{
var user={"username":req.body.username,"newpassword":req.body.newpassword};
//request validation
var validate=(user)=>
{
  const joischema=joi.object(
  {
  username:joi.string().required(),
  newpassword:joi.string().required()
  }).options({abortEarly:false});
  return joischema.validate(user);
}
response=validate(user);
if(response.error)
{
res.status(400).json({"Error":response.error.details[0].message})
}
else
{
//check the username
const getusername=()=>new Promise((resolve,reject)=>{
model.query("select username from admin where username='"+req.body.username+"'",(err,result)=>{
if(err) throw res.status(400).json({"Error":err});
if(result=="")
{
res.status(404).json({status:"false",Message:"User Not Found"});
}
else
{
resolve(result)
}
})
});
//update password
const updatepassword=()=>new Promise((resolve,reject)=>{
model.query("update admin set password='"+req.body.newpassword+"' where username='"+req.body.username+"'",(err,result)=>{
if(err) throw res.status(400).json({"Error":err});
resolve();
})
});


getusername().
then(updatepassword).
then(()=>res.status(200).json({status:"true",Message:"Password Changed"})).
catch((exception)=>res.status(400).json({status:"false",Message:exception}));

}
}
// controller for generate otp
const generateOtp=(req,res)=>
{

var otp=1234;
var user={username:req.body.username};
//request validation
var validate=(user)=>
{
const joischema=joi.object({username:joi.string().regex(/^([0-9])+$/).min(10).max(10).required()}).options({abortEarly:false});
return joischema.validate(user);
}

response=validate(user);
if(response.error)
{
res.status(400).json({"Error":response.error.details[0].message})
}
else
{
//otp insertion and
var promise1= new Promise((resolve,reject)=>
{

model.query("insert into user (user_id,otp,mobileno,status) values (0,'"+otp+"','"+req.body.username+"',null)",(err,result)=>{

if(err) res.status(400).json({"Error":err});
resolve();
});

});


promise1.then(()=>res.status(200).json({status:"true",Message:"OTP sent Successfully"})).
catch((err)=>{res.status(400).json({status:"false",Error:err})})


}



}
// controller for verifyinf OTP
const verifyOtp=(req,res)=>
{
const user={username:req.body.username,otp:req.body.otp};
//request validation
var validate=(user)=>
{
  const joischema=joi.object(
  {
  username:joi.string().required(),
  otp:joi.string().regex(/^([0-9]{4})+$/).min(4).max(4).required()
  }).options({abortEarly:false});
  return joischema.validate(user);
}
response=validate(user);
if(response.error)
{
res.status(400).json({"Error":response.error.details[0].message})
}
else
{
// check the mobileno
var getmobile=()=>new Promise((resolve,reject)=>
{

model.query("select mobileno from user where mobileno='"+req.body.username+"'",(err,result)=>
{
if(err) throw res.status(400).json({"Error":err});

if(result=="")
{
res.status(404).json({Status:"false",Message:"UserName Not Found! "})
}
else
{
resolve();
}
});
});

//  check the otp
var getotp=()=> new Promise((resolve,reject)=>
{
model.query("select otp from user where mobileno='"+req.body.username+"' and otp='"+req.body.otp+"'",(err,data)=>{

if(err) throw res.status(400).json({"Error":err});

if(data=="")
{
res.status(404).json({status:"false",Message:"Invalid OTP"});
}
else
{
resolve();
}
});

});
// status Updating
var updatestatus=()=>new Promise((resolve,reject)=>{
var status="Active";
model.query("update user set status='"+status+"' where mobileno='"+req.body.username+"'",(err,result)=>{
if(err) res.status(400).json({"Error":err});
resolve(result);
});
});
getmobile().
then(getotp).then(updatestatus).then(()=>res.status(200).json({status:"true",Message:"Login success"})).
catch((err)=>{res.status(400).json({status:"false",Error:err});})
}

}
module.exports={changePassword,adminLogin,generateOtp,verifyOtp}   

var mongo=require("mongodb").MongoClient;
var url="mongodb://localhost:27017/student_details"
var Joi=require("joi")
var Jwt=require("jsonwebtoken")
var config=require("./config.json");
var DB;
mongo.connect(url,function(err,db){
    if(err) throw err;
    console.log("Connected");
     DB=db.db("student_details")
})
//controller for user_signup
var user_signup=(req,res)=>{
    var stu_id;
    const user={id:req.body.id,username:req.body.username,phone_number:req.body.phone_number,
        address:req.body.address,password:req.body.password,class:req.body.class,year:req.body.year} 

    const validate=(user)=>{

        const joischema=Joi.object({
            id:Joi.string().min(1).max(1).required(),
            username:Joi.string().required(),
            phone_number:Joi.string().regex(/^[0-9]+$/).min(10).max(10).required(),
            address:Joi.string().required(),
        
            password:Joi.string().min(5).max(10).required(),
            class:Joi.string().min(1).max(1).required(),
            year:Joi.string().regex(/^[0-9]+$/).min(1).max(1).required()
        }).options({abortEarly:false})
        return joischema.validate(user);
    }
    response=validate(user);
    if(response.error)
    {
        res.status(400).json({"Error":response.error.details[0].message})
    
    }
    else{
        DB.collection("student").findOne({phone_number:req.body.phone_number},(err,data)=>{

            if(err) throw res.status(400).json({"status":"false","Error":err});
            if(data!=null)
            {
                DB.collection("student_class").insertOne({student_id:req.body.id,class_id:req.body.class,academic_id:req.body.year},function(err,data){
                    if(err) throw res.status(400).json({"status":"false","Error":err});
                    res.status(200).json({"status":"true","Message":"Sign sucessfull"})
                  
                });

            }
            else
            {


                var token=Jwt.sign(req.body.password,config.secretkey);

        var promise1=()=>new Promise((resolve,reject)=>{

            DB.collection("student").insertOne({_id:req.body.id,name:req.body.username,phone_number:req.body.phone_number,address:req.body.address,password:token},function(err,data){
                if(err) throw res.status(400).json({"status":"false","Error":err})
            resolve()
            })
        })
        var promise2=()=>new Promise((resolve,reject)=>{

            DB.collection("student").findOne({phone_number:req.body.phone_number},function(err,data){
                if(err) throw res.status(400).json({"status":"false","Error":err})
                stu_id=data._id
                resolve(stu_id)
            })
        })
        var promise3=()=>new Promise((resolve,reject)=>{

            DB.collection("student_class").insertOne({student_id:req.body.id,class_id:req.body.class,academic_id:req.body.year},function(err,data){
                if(err) throw res.status(400).json({"status":"false","Error":err})
                resolve("signup successfull")

              
            })
        })
        promise1().then(promise2).then(promise3).then((output=>{
            res.json({"Stats":"true",message:output})
        }))
        .catch(err=>{
            res.json({"Stats":"false","Error":+err})
        })


            }
        });
        //res.json({"status":"true",message:"validated successfully"})
        
        //res.status(404).json({"status":"false",message:"invalid phone_number"})
    
    
    }
}
//controller for user_login
var user_login=(req,res)=>{
    var verify_password;
    var student_id;
    var id;
    var finalarr=[]
    var user={phone_number:req.body.phone_number,password:req.body.password};
    var validate=(user)=>{
        
        const joischema=Joi.object({ 
        phone_number:Joi.string().regex(/^[0-9]+$/).min(10).max(10).required(),
        password:Joi.string().min(5).max(10).required()
         }).options({abortEarly:false})
         return joischema.validate(user)

    }
    response=validate(user);
    if(response.error){
        res.status(400).json({"Error":response.error.details[0].message})
    }
    else{
        verify_password=Jwt.sign(req.body.password,config.secretkey);
        var promise1=()=>new Promise((resolve,reject)=>{

            DB.collection("student").findOne({phone_number:req.body.phone_number},function(err,data){
                if(err) throw res.status(400).json({"status":"false","Error":+err})
                //student_id=JSON.parse(JSON.stringify(data.password));
                
                if(data==null){
                    res.status(401).json({"status":"false",message:"invalid username"})
                }
                else{
                    //console.log(data)
                    student_id=data.password
                //console.log(student_id)
                //console.log(verify_password)
                if(student_id==verify_password){
                    
                    resolve()
                }
                else{
                    res.status(401).json({"status":"false",message:"invalid password"})
                }
                    

                }
            })
        })
        var promise2=()=>new Promise((resolve,reject)=>{

            DB.collection("student").findOne({phone_number:req.body.phone_number},function(err,data){
                if(err) throw res.status(400).json({"status":"falese","Error":+err})
                id=data._id
                resolve(id)

            })
        })
        var promise3=()=>new Promise((resolve,reject)=>{

            DB.collection("student_class").aggregate([ { $match: {"student_id":id} },{
                $lookup:{
                    from:"class",
                    localField:"class_id",
                    foreignField:"id",
                    as:"class"
            
                }
            
            },{$lookup:{
                from:"academic",
                localField:"academic_id",
                foreignField:"id",
                as:"academic"
            
            
            
            }},{$lookup:{
                from:"student",
                localField:"student_id",
                foreignField:"_id",
                as:"student"
            }}
               ]).toArray(function(err,data){
                   if(err) throw res.status(400).json({"status":"false","Error":+err})
                //res.json(data)
                //var arr1=JSON.stringify(data);
                console.log(JSON.stringify(data))
            
                for(var i=0;i<data.length;i++){
                var myobj={name:data[i].student[0].name,
                phone_number:data[i].student[0].phone_number,
                address:data[i].student[0].address,
                class:data[i].class[0].class,
                year:data[i].academic[0].year
                }
                //console.log(myobj)
                finalarr.push(myobj);
                }
                resolve(finalarr)
                
                //var arraysingle=Array.prototype.concat.apply([],arr1)
            })
        })


    }
    
    promise1().then(promise2).then(promise3).then(output=>{
            console.log(output)
        res.json(output)})
    
    .catch(err=>{
        res.status(400).json({"status":"false","Error":+err})
    })

}
//exporting controllers
module.exports={user_signup,user_login}
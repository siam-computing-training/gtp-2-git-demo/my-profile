const express=require("express");
const app=express()
var Bodyparser=require("body-parser");

var router=express.Router()
var controller=require("./controller");
app.use(Bodyparser.urlencoded({extended:true}))
app.use(Bodyparser.json())
//url for signup
router.post("/signup",controller.user_signup);
//url for login
router.post("/login",controller.user_login);
app.use("/student",router)
app.listen(8086,()=>{console.log("Server Running On Port 8086")})

var express=require("express");
var app=express();
var router=express.Router()
var controller=require("./controller");
var Bodyparser=require("body-parser")
app.use(Bodyparser.json());
app.use(Bodyparser.urlencoded({extended:false}))
router.post("/student_details",controller.student_details);
router.post("/login",controller.student_login);
app.use("/student",router);
app.listen(8086,()=>{console.log("Server Running On Port 8086")})
var express=require("express");
var app=express()
var Joi=require("joi");
var config=require("./config.json");
var Jwt=require("jsonwebtoken");
var model=require("./model").connection;
var student_details=(req,res)=>{
    const user={username:req.body.username,phone_number:req.body.phone_number,
    address:req.body.address,password:req.body.password,class:req.body.class,year:req.body.year}          
const validate=(user)=>{
    const joischema=Joi.object({
        username:Joi.string().required(),
        phone_number:Joi.string().regex(/^[0-9]+$/).min(10).max(10).required(),
        address:Joi.string().required(),
        password:Joi.string().min(5).max(10).required(),
        class:Joi.string().min(1).max(1).required(),
        year:Joi.string().regex(/^[0-9]+$/).min(1).max(1).required()
    }).options({abortEarly:false})
    return joischema.validate(user);
}
response=validate(user);
if(response.error)
{
    res.status(400).json({"Error":response.error.details[0].message})

}
else{
    model.query("select id from student where phone_number='"+req.body.phone_number+"'",function(err,data){
        if(data=="")
        {
            var token=Jwt.sign(req.body.password,config.secretkey);
    var  student_id;
    var promise1=()=>new Promise((resolve,reject)=>
    {
        model.query("insert into student(id,name,phone_number,address,password) values(0,'"+req.body.username+"','"+req.body.phone_number+"','"+req.body.address+"','"+token+"')",function(err,result)
        {
            if(err) throw res.status(400).json({"status":"false","Error":err})
            resolve()

        });
   


    });

    var promise2=()=>new Promise((resolve,reject)=>
    {
        model.query("select id from student where phone_number='"+req.body.phone_number+"'",function(err,data)
        {
            if(err) throw res.status(400).json({"status":"false","Error":err})
            student_id=JSON.parse(JSON.stringify(data[0].id))
            resolve(student_id);
            
        })
    })
        var promise3=()=>new Promise((resolve,reject)=>{
            
            model.query("insert into student_class(id,student_id,class_id,academic_id) values (0,'"+student_id+"','"+req.body.class+"','"+req.body.year+"')",function(err,data){
                if(err) throw res.status(400).json({"status":"false","Error":err})
                resolve("success")

            })

        })
promise1().then(promise2).then(promise3).then((output)=>{
    res.json({"status":"true",message:"login successful"+output})
})
.catch(err=>{
    res.json({"status":"true",message:"login failed"+err})
})
res.json({"status":"true",message:"signup successful"})
    
    //console.log(token);


        }
        else{
            
    model.query("insert into student_class(id,student_id,class_id,academic_id) values (0,'"+JSON.parse(JSON.stringify(data[0].id))+"','"+req.body.class+"','"+req.body.year+"')",function(err,data){
        if(err) throw res.status(400).json({"status":"false","Error":err})
        res.status(200).json({"status":"true",message:"signup successfullly"})
        
            })

        }
    })
    }
}


var student_login=(req,res)=>{
    var verify_password;
    var student_id;
    var user={phone_number:req.body.phone_number,password:req.body.pass};
    var validate=(user)=>{
        const joischema=Joi.object({ 
        phone_number:Joi.string().regex(/^[0-9]+$/).min(10).max(10).required(),
         password:Joi.string().min(5).max(10).required()
         }).options({abortEarly:false})
         return joischema.validate(user)

    }
    response=validate(user);
    if(response.error){
        res.status(400).json({"Error":response.error.details[0].message})
    }
    else{
        verify_password=Jwt.sign(req.body.pass,config.secretkey);
        var promise2=()=>new Promise((resolve,reject)=>{
            var query="select password from student where phone_number='"+req.body.phone_number+"'"
            model.query(query,function(err,data){
             if(err) throw res.status(400).json({"status":"false","Error":err})
             if(data==""){
                 res.status(401).json({"status":"false",message:"invalid username"})
             }
             else{
                student_id=JSON.parse(JSON.stringify(data[0].password))
                console.log(student_id)
                console.log(verify_password)
                if(student_id==verify_password){
                    resolve()
                }
                else{
                    res.status(401).json({"status":"false",message:"invalid password"})
                }
                 
             }
            })
        
        
        })
       var promise3=()=>new Promise((resolve,reject)=>{
        model.query("select stu.name,stu.phone_number,stu.address,class.class,academic.year from student as stu inner join student_class on stu.id=student_class.student_id inner join academic on academic.id=student_class.academic_id inner join class on class.id=student_class.class_id where phone_number='"+req.body.phone_number+"'"
        ,function(err,result){
            if(err) throw err;
            if(result==""){
                res.status(404).json({"message":"data not found"})
            }
            else{
                resolve(result)
                
            }
        })
//


       })
    }
    promise2().then(promise3).then(data=>{
        res.status(200).json(data)
    })
}


module.exports={student_details,student_login}